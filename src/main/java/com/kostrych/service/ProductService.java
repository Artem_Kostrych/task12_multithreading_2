package com.kostrych.service;

import com.kostrych.wire.GetHandleProductIdsResponse;
import com.kostrych.wire.GetProductInfoResponse;

import java.util.List;

public interface ProductService {

    GetProductInfoResponse getProductInfo(String productId);

    GetHandleProductIdsResponse handleProducts(List<String> productIds);

    void stopProductExecutor();

}
